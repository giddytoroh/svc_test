"""
uility for i/o and data preservation
"""
import pickle
import librosa

def save_obj(obj, filename):
    """save python object"""
    with open(filename, 'wb') as handle:
        pickle.dump(obj, handle, protocol=pickle.HIGHEST_PROTOCOL)

def load_obj(filename):
    """load python object"""
    with open(filename, 'rb') as handle:
        obj = pickle.load(handle)
    return obj

def load_audio(wavfilename, mono, sr=16000, start=0, duration=None):
    """
    Only use the function in the context of source and target being
    MIR-1k and our newly compiled pro-singing.
    NOTE: mono==True corresp. pro-singing; mono==False corresp. source
    """
    assert mono in [True, False]
    audio, _ = librosa.load(wavfilename, sr=sr, mono=mono, \
                            offset=start, duration=duration)
    if not mono:
        sing = audio[1]
        bgm = audio[0]
        return sing, bgm
    else:
        return audio
